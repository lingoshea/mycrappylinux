#!/bin/bash
qemu-system-x86_64   -kernel $1 -initrd initrd.img -m 512 --enable-kvm -cpu host -nographic -append "console=ttyS0 nokaslr"  -s -S
